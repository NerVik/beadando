package snake;

import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.awt.event.*;
import java.awt.Color;

import java.util.ArrayList;
import java.util.Random;

import javax.swing.*;

public class Game extends JFrame implements KeyListener{
	
	private Point posHead;
	private ArrayList<Point> body;
	private ArrayList<Point> rocks;
	private Point food;
	private int currfood;
	private Timer t;
	private Direction d;
	private boolean ate;
	private Random rand;
	private Database db;
	
	private ArrayList<ArrayList<JButton>> tiles;
	
	public void keyPressed(KeyEvent e) {
    }

    public void keyReleased(KeyEvent e) {
        if(e.getKeyCode()== KeyEvent.VK_D && d != Direction.LEFT) {
            d = Direction.RIGHT;
        }
        else if(e.getKeyCode()== KeyEvent.VK_A && d != Direction.RIGHT) {
            d = Direction.LEFT;
        }
        else if(e.getKeyCode()== KeyEvent.VK_S && d != Direction.UP) {
            d = Direction.DOWN;
        }
        else if(e.getKeyCode()== KeyEvent.VK_W && d != Direction.DOWN) {     	
            d = Direction.UP;
        }

    }
    public void keyTyped(KeyEvent e) {
    }
	
	public Game() {
		rand = new Random();
		addKeyListener(this);
	}
	
	public void init() {
		try {
			this.db = new Database();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		this.setLayout(new GridLayout(2, 1));
		this.generateMenuBar();

		this.setTitle("Snake");
		this.setSize(500, 500);
		this.setVisible(true);
	}
	
	public void run() {
		this.init();
	}
	
	public void startNewGame() {
		if(t != null) {
			t.stop();
		}
		this.getContentPane().removeAll();
		
		this.getContentPane().setLayout(new GridLayout(21, 21));
		
		this.tiles = new ArrayList<ArrayList<JButton>>();
		ArrayList<JButton> row;
		for(int i = 0; i < 21; i++) {
			row = new ArrayList<JButton>();
			for(int j = 0; j < 21; j++) {
				JButton b = new JButton();
				//b.setSize(30, 30);
				b.setEnabled(false);
				//b.setText("-");
				this.getContentPane().add(b);
				row.add(b);
			}
			this.tiles.add(row);
		}
		this.revalidate();
		this.repaint();
		//this.pack();
		this.setSize((21)*20, 100 + 21*20);
		this.setVisible(true);
		
		this.runGame();
	}
	
	public void runGame() {
		this.ate = false;
		this.posHead = new Point(10, 10);
		this.body = new ArrayList<Point>();
		this.rocks = new ArrayList<Point>();
		int r = rand.nextInt(4);
		switch(r) {
		case 0:
			this.d = Direction.DOWN;
			break;
		case 1:
			this.d = Direction.UP;
			break;
		case 2:
			this.d = Direction.LEFT;
			break;
		case 3:
			this.d = Direction.RIGHT;
			break;
		}
		this.body.add(new Point(9, 10));
		
		this.food = new Point(this.rand.nextInt(21), this.rand.nextInt(21));
		while(this.body.contains(food) || this.posHead.equals(food)) {
			food = new Point(this.rand.nextInt(21), this.rand.nextInt(21));
		}
		
		for(int i = 0; i < 7; i++) {
			Point tmp = new Point(this.rand.nextInt(21), this.rand.nextInt(21));
			while(this.body.contains(tmp) || this.rocks.contains(tmp) || this.posHead.equals(tmp) || this.food.equals(tmp)) {
				tmp = new Point(this.rand.nextInt(21), this.rand.nextInt(21));
			}
			this.rocks.add(tmp);
		}
		currfood = 0;
		ActionListener taskPerformer = new ActionListener() {
		    public void actionPerformed(ActionEvent evt) {
		        step();
		    }
		};
		this.paintMap();
		t = new Timer(100, taskPerformer);
		t.start();
	}
	
	public void step() {
		if(this.posHead.equals(food)) {
			this.currfood++;
			ate = true;
			while(this.body.contains(food) || this.posHead.equals(food) || this.rocks.contains(food)) {
				food = new Point(this.rand.nextInt(21), this.rand.nextInt(21));
			}
		}
		if(!ate) {
			this.body.remove(this.body.size()-1);
		}
		else {
			ate = false;
		}
		switch(d) {
		case LEFT:
			if(this.posHead.getY() == 0) {
				this.endGame();
			}
			else {
				this.body.add(0, this.posHead);
				this.posHead = new Point(this.posHead.getX(), this.posHead.getY()-1);
			}
			break;
		case RIGHT:
			if(this.posHead.getY() == 20) {
				this.endGame();
			}
			else {
				this.body.add(0, this.posHead);
				this.posHead = new Point(this.posHead.getX(), this.posHead.getY()+1);
			}
			break;
		case UP:
			if(this.posHead.getX() == 0) {
				this.endGame();
			}
			else {
				this.body.add(0, this.posHead);
				this.posHead = new Point(this.posHead.getX()-1, this.posHead.getY());
			}
			break;
		case DOWN:
			if(this.posHead.getX() == 20) {
				this.endGame();
			}else {
				this.body.add(0, this.posHead);
				this.posHead = new Point(this.posHead.getX()+1, this.posHead.getY());
			}
			break;
		}
		if(this.body.contains(this.posHead) || this.rocks.contains(this.posHead)) {
			this.endGame();
		}
		this.paintMap();
	}
	
	public void paintMap() {
		for(int i = 0; i < this.tiles.size(); i++) {
			for(int j = 0; j < this.tiles.get(0).size(); j++) {
				this.tiles.get(i).get(j).setBackground(new Color(100, 100, 100));
			}
		}
		this.tiles.get(this.food.getX()).get(this.food.getY()).setBackground(new Color(255, 0, 0));
		for(int i = 0; i < this.rocks.size(); i++) {
			this.tiles.get(this.rocks.get(i).getX()).get(this.rocks.get(i).getY()).setBackground(new Color(255, 255, 0));
		}
		for(int i = 0; i < this.body.size(); i++) {
			this.tiles.get(this.body.get(i).getX()).get(this.body.get(i).getY()).setBackground(new Color(0, 250, 50));
		}
		this.tiles.get(this.posHead.getX()).get(this.posHead.getY()).setBackground(new Color(0, 150, 0));
	}
	
	public void showStatistics() {
		try {
			JOptionPane.showMessageDialog(null, db.getTopTen());
		} catch (HeadlessException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void endGame() {
		this.t.stop();
		String name = JOptionPane.showInputDialog("V�ge a j�t�knak. N�v: ");
		try {
			db.addRow(name, currfood);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void generateMenuBar() {
		JMenuBar mb = new JMenuBar();
		JMenu m = new JMenu("Men�");
		mb.add(m);
		JMenuItem ng = new JMenuItem("�j j�t�k");
		ng.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				startNewGame();
			}
		});
		
		JMenuItem st = new JMenuItem("Statisztik�k");
		st.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				showStatistics();
			}
		});
		
		JMenuItem kl = new JMenuItem("Kil�p�s");
		kl.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				System.exit(0);
			}
		});
		m.add(ng);
		m.add(st);
		m.add(kl);

		this.setJMenuBar(mb);
	}
}
