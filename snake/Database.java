package snake;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Database {
	private Connection con;
	private ResultSet result;
	private String top = "Top 10 j�t�kos: \n";
	private PreparedStatement statement;
	
	public Database() throws SQLException {
		con = DriverManager.getConnection("jdbc:mysql://localhost/snake?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC","root", "");
	}
	
	public String getTopTen() throws SQLException {
		top = "Top 10 j�t�kos: \n";
		statement = con.prepareStatement("select name, points from records order by points desc");
		result = statement.executeQuery();
		
		int i = 0;
		while(result.next() && i < 10) {
			top = top + result.getString(1) + ":" + result.getString(2) + "\n";
			i++;
		}
		
		return top;
	}
	
	public void addRow(String name, int points) throws SQLException {
		statement = con.prepareStatement("insert into records(name, points) values('" + name + "'," + points +")");
		statement.execute();
	}
}
